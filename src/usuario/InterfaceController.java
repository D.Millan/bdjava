/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


public class InterfaceController implements Initializable {
    
    private Usuario u = new Usuario();
    
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Button button1;
    @FXML
    private TextField id;
    @FXML
    private TextField nome;
    @FXML
    private TextField email;
    @FXML
    private PasswordField senha;
    @FXML
    private TextArea avisos;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cadastrar(ActionEvent event) {
        try{
        u.cadastrar(Integer.parseInt(id.getText()), nome.getText(), email.getText(), ""+senha.getText().hashCode());
        avisos.setText("Cadastro efetuado");
        }
        catch(Exception e)
        {
            avisos.setText(e.getMessage());
        }
    }

    @FXML
    private void buscar(ActionEvent event) {
        Main.trocaTela("TudoInterface.fxml");
    }
    
}
