/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class Usuario {

    private static final Connection c = Conexao.getConexao();
    private int id = 0, idAntigo;
    private String nome, email, senha; 
    
    static List<Usuario> getWhere(String condition) throws SQLException {
        List users = new ArrayList();
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM USUARIO WHERE " + condition + "ORDER BY ID");
        
        while(rs.next())
        {
            Usuario novo = new Usuario();
            novo.setId(rs.getInt("id"));
            novo.setNome(rs.getString("nome"));
            novo.setEmail(rs.getString("email"));
            novo.setSenha(rs.getString("senha"));
            users.add(novo);
        }
        
        return users;
    }
    
    public void update() throws SQLException {
        String sql =  "UPDATE usuario SET nome = ?, email = ?, id = ? WHERE ( id = ?)";///?????
        try{
            PreparedStatement ps = c.prepareStatement(sql);
            
            ps.setInt(4, this.idAntigo); //Observe o indice da chave primaria???
            
            ps.setString(1, this.nome);
            ps.setString(2, this.email);
            ps.setInt(3, this.id);
            ps.executeUpdate();
            
        }catch(SQLException e){            
            System.out.println("deu erro");
            e.printStackTrace();
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    
    public static List<Usuario> getAll() throws SQLException {
        List users = new ArrayList();
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM USUARIO ORDER BY ID");
        
        while(rs.next())
        {
            Usuario novo = new Usuario();
            novo.setId(rs.getInt("id"));
            novo.setNome(rs.getString("nome"));
            novo.setEmail(rs.getString("email"));
            novo.setSenha(rs.getString("senha"));
            users.add(novo);
        }
        
        return users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.idAntigo = this.id;
        this.id = id;
    }
    

    public Connection getC() {
        return c;
    }

    void cadastrar(int id, String n, String e, String s) throws SQLException {
        PreparedStatement ps = c.prepareStatement("INSERT INTO USUARIO(ID, NOME, EMAIL, SENHA) VALUES (?, ?, ?, ?)");
        ps.setInt(1, id);
        ps.setString(2, n);
        ps.setString(3, e);
        ps.setString(4, s);
        ps.execute();
    }
    
    
}
