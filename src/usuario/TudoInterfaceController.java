/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import java.net.URL;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TudoInterfaceController implements Initializable {

    private Usuario u = new Usuario();
    
    @FXML
    private AnchorPane entrada;
    @FXML
    private TableView<Usuario> tabela;
    @FXML
    private TableColumn<?, ?> id;
    @FXML
    private TableColumn<?, ?> nome;
    @FXML
    private TableColumn<?, ?> email;
    @FXML
    private TableColumn<?, ?> senha;
    @FXML
    private Button buscar;
    @FXML
    private TextField busca, idT, nomeT, emailT;
    @FXML
    private CheckBox tudo;
    @FXML
    private Button voltar;
    @FXML
    private Button buscar1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    private void addItems() throws SQLException{
       if(!tudo.isSelected()){
          for (Usuario u2 : Usuario.getWhere(busca.getText())) {
            tabela.getItems().add(u2);
        } 
       }
       else{
        for (Usuario u2 : Usuario.getAll()) {
            tabela.getItems().add(u2);
        }
       }
    }
    
    @FXML
    private void buscar(ActionEvent event) throws SQLException {
        try{
            tabela.getItems().clear();
            this.addItems();
            this.makeColumns();
            tabela.refresh();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }       
    }
    
    private void makeColumns() {
        id.setCellValueFactory(new PropertyValueFactory<>("id"));//precisa ter um getNome    
        nome.setCellValueFactory(new PropertyValueFactory<>("nome"));//precisa ter um getNome    
        email.setCellValueFactory(new PropertyValueFactory<>("email"));//precisa ter um getNome    
        senha.setCellValueFactory(new PropertyValueFactory<>("senha"));

    }

    @FXML
    private void voltar(ActionEvent event) {
         Main.trocaTela("Interface.fxml");
    }

    @FXML
    private void catchUser(MouseEvent event) {
        u = tabela.getSelectionModel().getSelectedItem();
        emailT.setText(u.getEmail());
        nomeT.setText(u.getNome());
        idT.setText(""+u.getId());
        
    }
 
    @FXML
    private void atualizar() {
        try{
            u.setId(Integer.parseInt(idT.getText()));
            u.setEmail(emailT.getText());
            u.setNome(nomeT.getText());
            u.update();
            tabela.refresh();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }   

}
